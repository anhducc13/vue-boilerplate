import axios from 'axios'
import store from '../store'
import { notification } from 'ant-design-vue'

const TYPE_TOKEN = 'Bearer'
const client = axios.create({
  baseURL: process.env.VUE_APP_API_URL
})

client.interceptors.request.use(
  (config) => {
    const token = store.state.user.accessToken
    config.headers['Authorization'] = `${TYPE_TOKEN} ${token}`
    return config
  },
  (error) => {
    notification.error({
      message: 'Có lỗi xảy ra'
    })
    return Promise.reject(error.response.data)
  }
)

client.interceptors.response.use(
  (response) => {
    return response.data
  },
  (error) => {
    return Promise.reject(error.response.data)
  }
)

export default {
  client
}
