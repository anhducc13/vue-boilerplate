import requestService from './request'

export const getMemos = (params) => {
  return requestService.client.get('/diaries', { params })
}

export const shareMemo = (id, emails) => {
  return requestService.client.post(`/diaries/${id}/shares`, {
    emails: emails.map((el) => ({ action: 'view', email: el }))
  })
}
