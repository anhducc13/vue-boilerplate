import requestService from './request'

export const login = (params) => {
  return requestService.client.post('/auth/login', params)
}

export const getToken = async (idToken) => {
  return requestService.client.post('/auth/get-token', { idToken })
}

export const getUserInfo = () => {
  return requestService.client.get('/auth/me')
}
