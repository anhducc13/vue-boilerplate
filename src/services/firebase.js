import firebase from 'firebase'
// import '@firebase/app-check';

firebase.initializeApp({
  apiKey: process.env.VUE_APP_FIREBASE_KEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN
})

const ggProvider = new firebase.auth.GoogleAuthProvider()
ggProvider.addScope('https://www.googleapis.com/auth/userinfo.email')
ggProvider.addScope('https://www.googleapis.com/auth/userinfo.profile')

const fbProvider = new firebase.auth.FacebookAuthProvider()
fbProvider.addScope('email')
fbProvider.addScope('public_profile')
fbProvider.setCustomParameters({
  display: 'popup'
})

const appleProvider = new firebase.auth.OAuthProvider('apple.com')
appleProvider.addScope('email')
appleProvider.addScope('name')

// const appCheck = (firebase as any).appCheck();
// appCheck.activate(process.env.REACT_APP_RECAPCHA_PUB_KEY);

export { ggProvider, fbProvider, appleProvider }

export default firebase
