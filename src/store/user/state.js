export default {
  accessToken: sessionStorage.getItem('ACCESS_TOKEN'),
  userInfo: null,
  loading: false
}
