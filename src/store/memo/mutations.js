export default {
  SET_MEMO_LIST: (state, list) => {
    state.memoList = list
  },
  SET_TOTAL_ITEM: (state, total) => {
    state.totalItems = total
  },
  SET_LOADING: (state, loading) => {
    state.loading = loading
  }
}
