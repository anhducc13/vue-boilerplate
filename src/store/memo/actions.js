import { getMemos, shareMemo } from '../../services'
import { notification } from 'ant-design-vue'

export default {
  async fetchMemoList({ commit }, query = {}) {
    commit('SET_LOADING', true)
    try {
      const res = await getMemos(query)
      const list = res?.result?.diaries || []
      const totalItems = res?.result?.pagination?.totalItems || 0
      commit('SET_MEMO_LIST', list)
      commit('SET_TOTAL_ITEM', totalItems)
    } finally {
      commit('SET_LOADING', false)
    }
  },
  async shareMemo({ commit }, { id, emails }) {
    commit('SET_LOADING', true)
    try {
      await shareMemo(id, emails)
      notification.success({
        message: 'Đặt lịch gửi email thành công'
      })
    } catch (e) {
      notification.error({
        message: e
      })
      return Promise.reject(e)
    } finally {
      commit('SET_LOADING', false)
    }
  }
}
